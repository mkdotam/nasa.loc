<?php

namespace AppBundle\Services;

class NasaApiClient
{
    protected $token = "N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD";
    protected $url = "https://api.nasa.gov/neo/rest/v1";

    public function __construct()
    {
    }

    public function getLast3Days()
    {
        $res = json_decode(file_get_contents($this->getFeed(date('Y-m-d', strtotime('3 days ago')), date('Y-m-d'))));
        return $res->near_earth_objects;
    }

    protected function getFeed($startDay, $endDay)
    {
        $url = $this->url . "/feed";
        $arguments = [
            "api_key"    => $this->token,
            "start_date" => $startDay,
            "end_date"   => $endDay,
            "detailed"   => 'false'
        ];
        $params = http_build_query($arguments);

        $request = $url . "?" . $params;

        return $request;
    }

}
