<?php

namespace AppBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    protected $data = [
        [
            'id'           => 1,
            'date'         => '2017-06-20T00:00:00+00:00',
            'reference'    => '3775154',
            'name'         => '(2017 KT34)',
            'speed'        => 32892.211769429,
            'is_hazardous' => false
        ],
        [
            'id'           => 2,
            'date'         => '2017-06-20T00:00:00+00:00',
            'reference'    => '3775210',
            'name'         => '(2017 LG1)',
            'speed'        => 28040.155513988,
            'is_hazardous' => false
        ],
        [
            'id'           => 3,
            'date'         => '2016-06-20T00:00:00+00:00',
            'reference'    => '3763940',
            'name'         => '(2016 WR)',
            'speed'        => 44369.938051962,
            'is_hazardous' => true
        ],
    ];

    public function testDefault()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $example_data = ['hello' => 'world!'];
        $response_data = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertArraySubset($example_data, $response_data);

    }

    public function testHazardous()
    {
        $client = static::createClient();
        $client->request('GET', '/neo/hazardous');

        $example_data = $this->data[2];
        $response_data = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertArraySubset($example_data, $response_data[0]);

    }

    public function testFastest()
    {
        $client = static::createClient();
        $client->request('GET', '/neo/fastest');

        $example_data = $this->data[0];
        $response_data = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertArraySubset($example_data, $response_data[0]);

    }

    public function testFastestHazardous()
    {
        $client = static::createClient();
        $client->request('GET', '/neo/fastest?hazardous=true');

        $example_data = $this->data[2];
        $response_data = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertArraySubset($example_data, $response_data[0]);

    }
}
