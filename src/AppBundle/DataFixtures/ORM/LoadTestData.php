<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Neo;

class LoadTestData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $neos = [
            [
                'id'           => 1,
                'date'         => '2017-06-20',
                'reference'    => '3775154',
                'name'         => '(2017 KT34)',
                'speed'        => 32892.211769429,
                'is_hazardous' => 0
            ],
            [
                'id'           => 2,
                'date'         => '2017-06-20',
                'reference'    => '3775210',
                'name'         => '(2017 LG1)',
                'speed'        => 28040.155513988,
                'is_hazardous' => 0
            ],
            [
                'id'           => 1,
                'date'         => '2016-06-20',
                'reference'    => '3763940',
                'name'         => '(2016 WR)',
                'speed'        => 44369.938051962,
                'is_hazardous' => 1
            ],
        ];
        foreach ($neos as $n) {
            $neo = new Neo();
            $neo ->setName($n['name']);
            $neo ->setDate(new \DateTime($n['date']));
            $neo ->setReference($n['reference']);
            $neo ->setSpeed($n['speed']);
            $neo ->setIsHazardous($n['is_hazardous']);
            $manager->persist($neo);
        }

        $manager->flush();
    }
}
