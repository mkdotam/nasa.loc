<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class ApiController
 * @package AppBundle\Controller
 */
class ApiController extends FOSRestController
{
    /**
     * @Route("/", name="hello")
     * @Method({"GET"})
     */
    public function helloWorldAction()
    {
        $data = array(
          "hello" => "world!"
        );

        return $data;
    }

}
