<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class NeoController
 * @package AppBundle\Controller
 * @Route("/neo")
 */
class NeoController extends FOSRestController
{
    /**
     * @Route("/hazardous", name="neo_hazardous")
     * @Method({"GET"})
     */
    public function hazardousAction()
    {
        $em = $this->getDoctrine()->getManager();
        $neos = $em->getRepository("AppBundle:Neo")->findByIsHazardous(true);

        return $neos;
    }

    /**
     * @Route("/fastest", name="neo_fastest")
     * @Method({"GET"})
     */
    public function fastestAction(Request $request)
    {
        $isHazardous = (is_null($request->get('hazardous'))) ? false : (bool)$request->get('hazardous');
        $em = $this->getDoctrine()->getManager();
        $neo = $em->getRepository("AppBundle:Neo")->getFastest($isHazardous);

        return $neo;
    }

    /**
     * @Route("/best-year", name="neo_best_year")
     * @Method({"GET"})
     */
    public function bestYearAction(Request $request)
    {
        $isHazardous = (is_null($request->get('hazardous'))) ? false : (bool)$request->get('hazardous');
        $em = $this->getDoctrine()->getManager();
        $year = $em->getRepository("AppBundle:Neo")->getBestYear($isHazardous);

        return $year;
    }

    /**
     * @Route("/best-month", name="neo_best_month")
     * @Method({"GET"})
     */
    public function bestMonthAction(Request $request)
    {
        $isHazardous = (is_null($request->get('hazardous'))) ? false : (bool)$request->get('hazardous');
        $em = $this->getDoctrine()->getManager();
        $month = $em->getRepository("AppBundle:Neo")->getBestMonth($isHazardous);

        return $month;
    }

}
