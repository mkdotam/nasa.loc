NASA API
========

## Deployment ##

Please clone application to your computer. Create two databases for the application: production and test.
Then copy **app/config/parameters.yml.dist** to the same folder and put production database settings.
After which please edit **app/config/config_test.yml** with test database settings.

Then run open terminal move to the application directory and run following commands:


```

#! composer install

```

After which please populate our database from NASA Api using following command:

```
#! app/console nasa:get:data

```

You will see how many entries were created in our DB.

```

#! app/console doctrine:schema:create

```
Now you can start consuming API. 

For running unit test make sure you have installed phpunit and run **bin/run_tests.sh** from application root folder.

## API ##

We have following end-points in our API.

* get hello world json                              GET     /                        
* get hazardous NEO                                 GET     /neo/hazardous     
* get fastest (filter by hazardous)                 GET     /neo/fastest?hazardous=(true|false)
* get best year (filter by hazardous)               GET     /neo/best-year?hazardous=(true|false)
* get fastest (filter by hazardous)                 GET     /neo/best-month?hazardous=(true|false)

Take into account that default value for hazardous is false. 
And if you're not specifying hazardous arguments it doesn't mean that you will get info from both hazardous and non-hazardous asteroids, but only from non-hazardous. 